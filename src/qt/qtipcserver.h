#ifndef QTIPCSERVER_H
#define QTIPCSERVER_H

// Define coin-qt message queue name
#define BITCOINURI_QUEUE_NAME "coinURI"

void ipcScanRelay(int argc, char *argv[]);
void ipcInit(int argc, char *argv[]);

#endif // QTIPCSERVER_H
